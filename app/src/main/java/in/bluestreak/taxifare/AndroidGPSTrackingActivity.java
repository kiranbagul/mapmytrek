package in.bluestreak.taxifare;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class AndroidGPSTrackingActivity extends Activity {

    String list = "";
    Button btnShowLocation;
    TextView locationLog;

    GPSTracker gps;

    Timer timer;
    MyTimerTask myTimerTask;
    long prevDistance;
    double prevLat;
    double prevLong;
    TextView distanceView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        locationLog = (TextView)findViewById(R.id.locationList);
        distanceView = (TextView)findViewById(R.id.distance);
        distanceView.setTypeface(Typeface.SANS_SERIF);
        distanceView.setTextSize(50);

        if(timer != null){
            timer.cancel();
        }
        timer = new Timer();
        myTimerTask = new MyTimerTask();
        timer.schedule(myTimerTask, 1000, 5000);
        gps = new GPSTracker(AndroidGPSTrackingActivity.this);
        //  final ScrollView scroll = (ScrollView) findViewById(R.id.scroller);
        //scroll.fullScroll(View.FOCUS_DOWN);
        locationLog.setMovementMethod(new ScrollingMovementMethod());
        locationLog.setMaxLines(30);
    }


    class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            Calendar calendar = Calendar.getInstance();
            runOnUiThread(new Runnable(){

                @Override
                public void run() {
                    Log.d(" >>>>>>>> ", " >>>> ");
                    if(gps.canGetLocation()){
                        double latitude = gps.getLatitude();
                        double longitude = gps.getLongitude();
                        Calendar c = Calendar.getInstance();
                        int seconds = c.get(Calendar.SECOND);
                        int min = c.get(Calendar.MINUTE);
                        if(prevLat != 0){
                            prevDistance +=DistanceCalcUtils.distance(latitude, longitude, prevLat, prevLong, 'm');
                        }
                        list = list+"\n"+min+":"+seconds+" || Lt: "+ latitude + ", Lng: " + longitude;
                        if(prevDistance < 1000) {
                            distanceView.setText(prevDistance + "m");
                        }else{
                            double val = prevDistance/1000;
                            distanceView.setText( val+ " km ");
                        }
                        prevLat = latitude;
                        prevLong = longitude;
                    }else{
                        gps.showSettingsAlert();
                    }
                    locationLog.setText(list);
                }});
        }

    }

}